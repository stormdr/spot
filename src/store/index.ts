import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const state = {

	level: 0,

	radioactive: 0,

	colors: [    
		{
			name: "red",
			hash: "#ce1717",
			count: 0
		},
		{
      		name: "green",
			hash: "#1fd647",
			count: 0
	    	},
    		{
      		name: "blue",
			hash: "#3578f4",
			count: 0
    		},
  	],

	types: [
		{
			id: 1,
			name: "mdi-clippy",
			count: 0
		},
		{
			id: 2,
			name: "mdi-egg-easter",
			count: 0
		},
		{
			id: 3,
			name: "mdi-google-downasaur",
			count: 0
		},
		{
			id: 4,
			name: "mdi-pac-man",
			count: 0
		},
		{
			id: 5,
			name: "mdi-piggy-bank",
			count: 0
		},
		{
			id: 6,
			name: "mdi-pokeball",
			count: 0
		},
		{
			id: 0,
			name: "mdi-car-sports",
			count: 0
		}
	],
	
	animations: [
		{
			name: "downhill",
			alternate: true,
			race: false
		},
		{
			name: "uphill",
			alternate: true,
			race: false
		},
		{
			name: "zigzag-down",
			alternate: true,
			race: false
		},
		{
			name: "zigzag-up",
			alternate: true,
			race: false
		},
		{
			name: "bounce",
			alternate: true,
			race: false
		},
		{
			name: "line-top",
			alternate: false,
			race: false
		},
		{
			name: "line-middle",
			alternate: false,
			race: false
		},
		{
			name: "line-bottom",
			alternate: false,
			race: false
		},
		{
			name: "line-vertical",
			alternate: false,
			race: false
		},
		{
			name: "cross-down",
			alternate: false,
			race: false
		},
		{
			name: "cross-up",
			alternate: false,
			race: false
		},
		{
			name: "race-one",
			alternate: false,
			race: true
		},
		{
			name: "race-two",
			alternate: false,
			race: true
		},
		{
			name: "race-three",
			alternate: false,
			race: true
		}
	],

	categories: [
		{
			name: "amount",
			text: "There _T _A $",
			order: 1,
			random: true
		},
		{
			name: "color-count",
			text: "There _T1 _A _C _T2",
			order: 2,
			random: true
		},
		{
			name: "color-same",
			text: "All $ are _C",
			order: 3,
			random: false
		},
		{
			name: "missing",
			text: "There is no $",
			order: 4,
			random: true
		},
		/*{
			name: "radioactive",
			text: "_A _T radioactive",
			order: 5,
			random: true
		}*/
	],

	difficulties: [
		{
			start: 10,
			limit: 4
		},
		{
			start: 5,
			limit: 3
		},
		{
			start: 1,
			limit: 2
		},
	]
};

const mutations = {};

const actions = {};

const modules = {};

export default new Vuex.Store({
	state,
	mutations,
	actions,
	modules
})
