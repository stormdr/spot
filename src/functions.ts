import Limited from "./types/Limited";

export function random(range: number) {

      return Math.floor(Math.random() * range);

}

export function getRandomItem<T extends Limited>(list: T[], limit: number): T {
     
      const l = list.filter((i) => i.count < limit);
      let item = l[random(l.length)];

      return item;

}

export function takeRandomItem<T>(list: T[]): T {

      let item = list[random(list.length)];
      list.splice(list.indexOf(item), 1);

      return item;
      
}