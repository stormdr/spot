import Vue from "vue";
import VueRouter from "vue-router";
import App from "./App.vue";
import store from "./store";
const Menu = () => import("./views/Menu.vue");
const Game = () => import("./views/Game.vue");
const Race = () => import("./views/Race.vue");

Vue.config.productionTip = false

const routes = [
	{ path: "/", redirect: "/menu" },
	{ path: "/menu", name: "menu", component: Menu },
	{ path: "/game", name: "game", component: Game },
	{ path: "/race", name: "race", component: Race } 
];

const router = new VueRouter({
	routes
});

Vue.use(VueRouter);

new Vue({
	render: h => h(App),
	store,
	router
}).$mount("#app");
