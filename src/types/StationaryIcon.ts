import IconType from "./IconType";
import Icon from "./Icon";

export default class StationaryIcon implements Icon {

      type: IconType = new IconType;

}