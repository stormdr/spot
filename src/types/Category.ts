export default class AnswerCategory {

      name: string = "";

      text: string = "";

      order: number = 1;

      random: boolean = true;
            
}