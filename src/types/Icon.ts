import IconType from "./IconType";

export default interface Icon {

      type: IconType;
      
}