import Limited from "./Limited";

export default class IconType implements Limited {

      id: number = 0;

      name: string = "";

      count: number = 0;   

}