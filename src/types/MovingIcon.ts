import IconType from "./IconType";
import Color from "./Color";
import Animation from "./Animation";
import * as f from "../functions";
import store from "../store";
import Icon from "./Icon";

export default class MovingIcon implements Icon {

      type: IconType = new IconType;

      color: Color = new Color;

      animation: Animation = new Animation;

      speed: string = "0s";

      radioactive: boolean = false;

      faded: boolean = false;

      static setIcon(type: IconType, data: any, index: number, race: boolean): MovingIcon {            

            let color = f.getRandomItem<Color>(data.colors, 4);
            type.count++;
            color.count++;

            let speed = f.random(3) + 3 + (index / 10) - (data.level / 10);
            
            if(race) {
                  speed += 2;
            }

            //let radioactive = f.random(10) < 3 ? true : false;
            
            let icon: MovingIcon = {
                  type: type,
                  color: color,
                  animation: f.takeRandomItem<Animation>(data.animations),
                  speed: speed.toString() + "s",
                  radioactive: false,
                  faded: false
            };

            /*if(radioactive) {
                  store.state.radioactive += 1;
            }*/

            return icon;

      }

}