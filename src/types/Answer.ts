import Category from "./Category";
import IconType from "./IconType";
import Color from "./Color";
import Icon from "./MovingIcon";
import * as f from "../functions";
import store from "../store";

export default class Answer {

      category: Category = new Category;

      text: string = "";

      textAfter?: string;

      correct: boolean = false;

      iconType?: IconType = new IconType;      

      iconColor?: Color = new Color;

      iconAmount: number = 0;

      solutionVisible: boolean = false;

      static setAnswer(correct: boolean, data: any): Answer {

            let amount = 3;

            if(data.difficulty.limit < 3) {
                  amount = 2;
            }

            let answer: Answer = {
                  category: new Category,
                  text: "",
                  textAfter: "",
                  correct: correct,
                  iconType: undefined,
                  iconColor: undefined,
                  iconAmount: amount,
                  solutionVisible: false
            };

            this.colorSameCheck(answer, data);
            this.categoryCheck(answer, data);
            this.adjustText(answer);

            return answer;

      }

      static colorSameCheck(answer: Answer, data: any) {

            let answerSet = false;
            let isColorSame = false;
            let colorSameCategory = data.categories.filter((c: Category) => c.name === "color-same")[0];

            data.types.sort((a: IconType, b: IconType) => b.count - a.count).forEach((t: IconType) => {

                  if(!answerSet && t.count > 2) {

                        const iconList = data.icons.filter((i: Icon) => i.type === t);

                        iconList.forEach((icon: Icon) => {

                              if(!answerSet) {

                                    let equal = iconList.filter((i: Icon) => i.color === icon.color).length;
                                    
                                    if((answer.correct && equal === t.count) || (!answer.correct && equal === (t.count - 1))) {
                                          isColorSame = true;
                                    }

                                    if(isColorSame) {
                                          answer.iconType = t;
                                          answer.iconColor = icon.color;
                                          answerSet = true;
                                    }

                              }
                              
                        });

                  }         

            });

            if(isColorSame && colorSameCategory) {
                  answer.category = colorSameCategory;
                  data.categories.splice(data.categories.indexOf(answer.category), 1);
            }
            else {
                  answer.category = f.takeRandomItem<Category>(data.rCategories);
            }

      }

      static categoryCheck(answer: Answer, data: any) {

            let correction = 0;

            if(!answer.correct) {
                  correction = 1;
            }

            data.types.forEach((t: IconType) => {

                  let incorrect = answer.iconAmount - 1;

                  if(answer.iconAmount < 3) {
                        incorrect = answer.iconAmount + 1;
                  }

                  if(answer.correct) {

                        if(t.count === 0 && answer.category.name === "missing") {
                              answer.iconType = t;
                        }
                        else if(t.count === answer.iconAmount && answer.category.name === "amount") {
                              answer.iconType = t;
                        }

                  }
                  else {

                        if(t.count === 1 && answer.category.name === "missing") {
                              answer.iconType = t;
                        }
                        else if(t.count === answer.iconAmount && answer.category.name === "amount") {
                              answer.iconType = t;
                              answer.iconAmount = incorrect;
                        }

                  }
                  
            });

            data.colors.forEach((c: Color) => {

                  if(answer.category.name === "color-count") {

                        answer.iconColor = c;

                        if(c.count === 4) {
                              answer.iconAmount = c.count - correction;
                        }
                        else {
                              answer.iconAmount = c.count + correction;
                        }

                  }
                  
            });

            if(answer.category.name === "radioactive") {
                  if(answer.correct) {
                        answer.iconAmount = store.state.radioactive;
                  }
                  else {
                        if(store.state.radioactive < 4) {
                              answer.iconAmount = store.state.radioactive + 1;
                        }
                        else {
                              answer.iconAmount = store.state.radioactive - 1;
                        }
                  }
            }

      }

      static adjustText(answer: Answer) {

            let textColor = answer.iconColor ? answer.iconColor.name : "";
            let textAmount = answer.iconAmount !== undefined ? <string><unknown> answer.iconAmount : "";

            let text = answer.category.text.replace("_C", textColor).replace("_A", textAmount);

            if(answer.category.name === "amount") {
                  if(answer.iconAmount === 1) {
                        text = text.replace("_T", "is");
                  }
                  else {
                        text = text.replace("_T", "are");
                  }
            }

            if(answer.category.name === "color-count") {
                  if(answer.iconAmount === 1) {
                        text = text.replace("_T1", "is");
                        text = text.replace("_T2", "icon");
                  }
                  else {
                        text = text.replace("_T1", "are");
                        text = text.replace("_T2", "icons");
                  }
            }

            if(answer.category.name === "radioactive") {
                  if(answer.iconAmount === 1) {
                        text = text.replace("_T", "icon is");
                  }
                  else {
                        text = text.replace("_T", "icons are");
                  }
            }

            let $ = text.indexOf("$");

            if($ !== -1) {
                  answer.textAfter = text.substr($ + 1);
                  answer.text = text.substr(0, $);
            }
            else {
                  answer.text = text;
                  answer.iconType = undefined;
            }

      }
      
}