import Limited from "./Limited";

export default class Color implements Limited {

      name: string = "";

      hash: string = "";

      count: number = 0;

}